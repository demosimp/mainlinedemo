#include <iostream>
#include <string>
#include "networking.hh"
#include "utility.hh"
#include "assert.h"

using namespace std;

Networking::Networking()
{
  cout << "from Networking constructor "<< endl;
}
void Networking::Networking_method1()
{

  cout << "from Networking::Networking_method1"  << endl;
}
void Networking::Networking_method2()
{

  cout << "from Networking::Networking_method2"  << endl;
  cout << "enter value tobe supplied to utility method "  << endl;
  int input ;
  cin >> input ; 
  Utility objUtility;
  int retval = objUtility.Utility_method1(input);
  cout << "returned value is " << retval << endl;
  assert (retval == 10);
}
