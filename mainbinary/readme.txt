You compile the shared lib first:

$ g++ -Wall -g -fPIC -c cat.cpp
$ g++ -shared -Wl,-soname,libcat.so.1 -o libcat.so.1 cat.o
Then compile the main executable or C++ program using the classes in the libraries:

$ g++ -Wall -g -c main.cpp
$ g++ -Wall -Wl,-rpath,. -o main main.o libcat.so.1 # -rpath linker option prevents the need to use LD_LIBRARY_PATH when
testing
$ ./main
Meet my cat, Felix!
Meow! I'm Felix
$
i

https://stackoverflow.com/questions/58058/using-c-classes-in-so-libraries/58171





 g++ -g -Wall -o main -I /root/demo_setup_mainlinebuild/D1 -I /root/demo_setup_mainlinebuild/D2
 -L/root/demo_setup_mainlinebuild/D1 -L/root/demo_setup_mainlinebuild/D2 -lcat -lnetworking main.cpp
O

******************************************************************************************************************
compute
493  g++ -Wall -g -fPIC -c compute.cpp
    494  g++ -shared -Wl,-soname,libcompute.so.1 -o libcompute.so.1 compute.o

utility compilation 

g++ -Wall -g -fPIC -c utility.cpp
g++ -shared -Wl,-soname,libutility.so.1 -o libutility.so.1 utility.o


Networking compilation 

g++ -Wall -g -fPIC -I /root/demo_setup_mainlinebuild/utility -c networking.cpp
g++ -shared -Wl,-soname,libnetworking.so.1 -o libnetworking.so.1 networking.o


main binary compilation 

g++ -g -Wall -o main -I /root/demo_setup_mainlinebuild/D2  -I /root/demo_setup_mainlinebuild/D3  -I
/root/demo_setup_mainlinebuild/utility  -L/root/demo_setup_mainlinebuild/D2  -L/root/demo_setup_mainlinebuild/utility
-L/root/demo_setup_mainlinebuild/D3   -lcompute -lnetworking  -lutility main.cpp


******************************************************************************************************************

